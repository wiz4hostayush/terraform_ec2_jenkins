resource "aws_vpc" "my_new_vpc" {
  cidr_block       = var.vpc_cidr_block
  instance_tenancy = "default"
  enable_dns_hostnames = "true"
  tags = {
    Name = "new_vpc"
  }
}
resource "aws_subnet" "public_subnet" {
  vpc_id     = aws_vpc.my_new_vpc.id
  cidr_block = var.public_subnet_cidr_block
  availability_zone = var.public_subnet_availability_zone
  map_public_ip_on_launch = "true"
  tags = {
      Name = "subnet1public"
    }
}
resource "aws_subnet" "private_subnet" {
  vpc_id     = aws_vpc.my_new_vpc.id
  cidr_block = var.private_subnet_cidr_block
  availability_zone = var.private_subnet_availability_zone
  tags = {
      Name = "subnet2private"
    }
}
resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.my_new_vpc.id
  tags = {
      Name = "my_internetgateway"
    }
}
resource "aws_eip" "tf_eip" {
  depends_on = [ aws_instance.Jenkins_server ]
   vpc      = true
}
resource "aws_nat_gateway" "nat_gateway" {
  depends_on = [ aws_eip.tf_eip ]
  allocation_id = aws_eip.tf_eip.id
  subnet_id     = aws_subnet.public_subnet.id
  tags = {
    Name = "my_Nat_gateway"
  }
}
resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.my_new_vpc.id
  route {
      
  gateway_id = aws_internet_gateway.internet_gateway.id
      cidr_block = "0.0.0.0/0"
    }
  tags = {
      Name = "my_rt2"
    }
}
resource "aws_route_table_association" "association" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.route_table.id
}
resource "aws_route_table" "nat_route_table" {
  depends_on = [ aws_nat_gateway.nat_gateway ]
  vpc_id = aws_vpc.my_new_vpc.id
  route {    
    gateway_id = aws_nat_gateway.nat_gateway.id
    cidr_block = "0.0.0.0/0"
  }
    tags = {
    Name = "my_nat_route_table"
  }
}
resource "aws_route_table_association" "association2" {
  depends_on = [ aws_route_table.nat_route_table ]
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.nat_route_table.id
}