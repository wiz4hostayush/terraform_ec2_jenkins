#Terraform file terraform.tf
variable "aws_region" {
  type        = string
  description = "AWS region"
}

variable "profile_name" {
  type        = string
  description = "AWS profile name"
}

#Terraform file ec2.tf
variable "instance_ami" {
  type        = string
  description = "AMI ID"
}

variable "instance_type" {
  type        = string
  description = "EC2 instance type"
}

variable "key_name" {
  type        = string
  description = "EC2 key name"
}

variable "security_group_name" {
  type        = string
  description = "EC2 security group Name"
}

variable "cidr_ingress_rules_1" {
  type = list(object({
    from             = number
    to               = number
    protocol         = string
    cidr_blocks      = list(string)
    ipv6_cidr_blocks = list(string)
    description      = string
  }))
}

variable "cidr_ingress_rules_2" {
  type = list(object({
    from             = number
    to               = number
    protocol         = string
    cidr_blocks      = list(string)
    ipv6_cidr_blocks = list(string)
    description      = string
  }))
}

variable "cidr_egress_rules" {
  type = list(object({
    from             = number
    to               = number
    protocol         = string
    cidr_blocks      = list(string)
    ipv6_cidr_blocks = list(string)
    description      = string
  }))
}

#Terraform file vpc.tf
variable "vpc_cidr_block" {
  type        = string
  description = "CIDR block for the VPC"
}

variable "public_subnet_cidr_block" {
  type        = string
  description = "CIDR block for the public subnet"
}

variable "private_subnet_cidr_block" {
  type        = string
  description = "CIDR block for the private subnet"
}

variable "public_subnet_availability_zone" {
  type        = string
  description = "Availability zone for the public subnet"
}

variable "private_subnet_availability_zone" {
  type        = string
  description = "Availability zone for the private subnet"
}


#Terraform file Ansible.tf 
variable "instance_private_key" {
    type        = string
    description = "Private key for the instance"
}

