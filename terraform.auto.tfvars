#Account Info
aws_region   = "us-east-1"
profile_name = "____"#"wiz4host_new"

#EC2 Info
instance_type       = "t2.micro"
key_name            = "____"#"Ayush_DevOps_Engineer_India"
instance_ami        = "ami-06640050dc3f556bb"
security_group_name = "Jenkins_server_sg"

cidr_ingress_rules_1 = [
  {
    from             = 8080,
    to               = 8080,
    protocol         = "tcp",
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = []
    description      = "CCOE Ingress rules"
}]

cidr_ingress_rules_2 = [
  {
    from             = 22,
    to               = 22,
    protocol         = "tcp",
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = []
    description      = "CCOE Ingress rules"
  }
]

cidr_egress_rules = [{
  from             = 0,
  to               = 65535,
  protocol         = "tcp",
  cidr_blocks      = ["0.0.0.0/0"]
  ipv6_cidr_blocks = []
  description      = "CCOE egress rules"
}]

#VPC Info
vpc_cidr_block                   = "192.168.0.0/16"
public_subnet_cidr_block         = "192.168.0.0/24"
public_subnet_availability_zone  = "us-east-1a"
private_subnet_cidr_block        = "192.168.1.0/24"
private_subnet_availability_zone = "us-east-1b"

#Instance Install Info
instance_private_key = "E:/AWS KeyPairs/Ayush_DevOps_Engineer_India.pem"
