resource "null_resource" "ansible_setup" {
    depends_on = [
      aws_instance.Jenkins_server
    ]
    triggers = {
        cluster_instance_ids = join(",", aws_instance.Jenkins_server.*.id)
    }
    connection {
        type        = "ssh"
        user        = "ec2-user"
        private_key = file(var.instance_private_key)
        host = element(aws_instance.Jenkins_server.*.public_ip, 0)
    }
    provisioner "file" {
        source      = "./ansible/ansible.cfg"
        destination = "/home/ec2-user/ansible.cfg"
    }
    provisioner "file" {
        source      = "./ansible/local_setup.yaml"
        destination = "/home/ec2-user/local_setup.yaml"
    }

    provisioner "remote-exec" {
        inline = [
            "sudo amazon-linux-extras install epel -y",
            "sudo yum install ansible sshpass -y",
            "ansible-playbook /home/ec2-user/local_setup.yaml",
        ]
    }
}