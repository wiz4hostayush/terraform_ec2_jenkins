resource "aws_security_group" "default" {
  depends_on = [aws_vpc.my_new_vpc]
  name       = var.security_group_name
  vpc_id     = aws_vpc.my_new_vpc.id
}

resource "aws_security_group_rule" "cidr_ingress_1" {
  depends_on = [
    aws_security_group.default
  ]
  type              = "ingress"
  security_group_id = resource.aws_security_group.default.id
  for_each = {
  for ingress in var.cidr_ingress_rules_1 : "${ingress.description} ${ingress.from} ${ingress.to} ${ingress.protocol} ${ingress.cidr_blocks[0]}" => ingress }

  from_port        = each.value.from
  to_port          = each.value.to
  protocol         = each.value.protocol
  cidr_blocks      = each.value.cidr_blocks
  ipv6_cidr_blocks = each.value.ipv6_cidr_blocks
}

resource "aws_security_group_rule" "cidr_ingress_2" {
  depends_on = [
    aws_security_group.default
  ]
  type              = "ingress"
  security_group_id = resource.aws_security_group.default.id
  for_each = {
  for ingress in var.cidr_ingress_rules_2 : "${ingress.description} ${ingress.from} ${ingress.to} ${ingress.protocol} ${ingress.cidr_blocks[0]}" => ingress }

  from_port        = each.value.from
  to_port          = each.value.to
  protocol         = each.value.protocol
  cidr_blocks      = each.value.cidr_blocks
  ipv6_cidr_blocks = each.value.ipv6_cidr_blocks
}

resource "aws_security_group_rule" "cidr_egress" {
  depends_on = [
    aws_security_group.default
  ]
  type              = "egress"
  security_group_id = resource.aws_security_group.default.id
  for_each = {
  for egress in var.cidr_egress_rules : "${egress.description} ${egress.from} ${egress.to} ${egress.protocol} ${egress.cidr_blocks[0]}" => egress }

  from_port        = each.value.from
  to_port          = each.value.to
  protocol         = each.value.protocol
  cidr_blocks      = each.value.cidr_blocks
  ipv6_cidr_blocks = each.value.ipv6_cidr_blocks
}

/* SG
  resource "aws_security_group_rule" "security_group_ingress" {
    type              = "ingress"
    security_group_id = resource.aws_security_group.default.id
    for_each = {
    for ingress in var.security_group_ingress_rules : "${ingress.description} ${ingress.from} ${ingress.to} ${ingress.protocol} }" => ingress }
    from_port                = each.value.from
    to_port                  = each.value.to
    protocol                 = each.value.protocol
    source_security_group_id = each.value.source_security_group_id == "" ? aws_security_group.default.id : each.value.source_security_group_id
  }

  resource "aws_security_group_rule" "security_group_egress" {
    type              = "egress"
    security_group_id = resource.aws_security_group.default.id
    for_each = {
    for egress in var.security_group_egress_rules : "${egress.description} ${egress.from} ${egress.to} ${egress.protocol}" => egress }

    from_port                = each.value.from
    to_port                  = each.value.to
    protocol                 = each.value.protocol
    source_security_group_id = each.value.source_security_group_id == "" ? aws_security_group.default.id : each.value.source_security_group_id
  }
*/

resource "aws_instance" "Jenkins_server" {
  depends_on = [
    aws_security_group.default,
    aws_security_group_rule.cidr_ingress_1,
    aws_security_group_rule.cidr_ingress_2,
    aws_security_group_rule.cidr_egress,
  ] 
  ami                    = var.instance_ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.public_subnet.id
  vpc_security_group_ids = [aws_security_group.default.id]
  key_name               = var.key_name
}
